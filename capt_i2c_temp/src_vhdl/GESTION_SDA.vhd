-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : GESTION_SDA.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 19.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    19.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	-- Numeric
	use ieee.numeric_std.all;

	--------------
	-- Packages --
	--------------
	use work.CapT_I2C_pkg.all;

	------------
	-- Entity --
	------------
	entity GESTION_SDA is
		port (
		    com_SDA_i : in   std_logic;
			 sda_io		: inout std_logic;
			 rdx_sda_o : out  std_Logic
		    );
	end entity GESTION_SDA;

	------------------
	-- Architecture --
	------------------
	architecture struct of GESTION_SDA is
	
		signal sda_s	: std_logic;

	begin
	
		sda_s <= 'Z' when (com_SDA_i = '1') else '0';
		sda_io <= sda_s;
		rdx_sda_o <= to_x01(sda_io);
	
	end architecture struct;
