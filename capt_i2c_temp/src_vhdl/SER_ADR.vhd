-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : SER_ADR.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	-- Numeric
	use ieee.numeric_std.all;

	--------------
	-- Packages --
	--------------
	use work.CapT_I2C_pkg.all;

	------------
	-- Entity --
	------------
	entity SER_ADR is
		port (
		    clock_i   : in   std_logic;
		    reset_i	  : in   std_Logic;
		    addr_i	  : in   std_logic_vector(2 downto 0);
		    init_i    : in   std_logic;
		    en_wr_i   : in   std_logic;

		    wr_SDA_o : out  std_logic;
			 write_done_o : out std_logic
		    );
	end entity SER_ADR;

	------------------
	-- Architecture --
	------------------
	architecture struct of SER_ADR is
	
		signal next_address_s, address_s 		: std_logic_vector(8 downto 0) :=  "000000000";
		signal next_SDA_s, SDA_s 					: std_logic;
		signal next_bit_count, cur_bit_count	: unsigned (3 downto 0);
		
	begin
	
	process(clock_i, reset_i, init_i, address_s)
	begin
		if(reset_i = '1') then
			address_s	<= "000000000";
			cur_bit_count <= "0000";
		elsif(init_i = '1') then
			address_s <= 'Z' & Addr_LM75_c(6 downto 3) & addr_i & '1';
			cur_bit_count <= "0000";
		elsif(rising_edge(clock_i)) then
			address_s <= next_address_s;
			cur_bit_count <= next_bit_count;
		end if;
	end process;

	next_address_s <= address_s(7 downto 0) & '0' when en_wr_i = '1' else address_s;
	next_bit_count <= cur_bit_count + 1 when en_wr_i = '1' or init_i = '1' else
						 cur_bit_count;
	write_done_o 	<= '1' when cur_bit_count >= 8 else '0';
	
	wr_SDA_o <= address_s(8) when cur_bit_count < 9 else '0';

	end architecture struct;
