-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Fichier :  Shared_Counter_pkg.vhd
-- Auteur  :  E. Messerli
-- Date    :  10.03.2008
--
-- Utilise dans   : Labo CSF, Master I2C
-----------------------------------------------------------------------
-- Fonctionnement vu de l'exterieur : 
--   Paquetage des projets: FIFO, SRGN: Master_I2C
--
-----------------------------------------------------------------------
-- Ver  Date     Qui  Commentaires
-- 0.0  10.03.08 EMI  Version initiale pour le projet Capt_T_PCI
-- 1.0  03.03.10 GCD  Modification pour le projet FIFO (ajout ilog*)
-- 2.0  16.04.10 GCD  Modification du nom en Shared_Counter_pkg pour
-----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package Shared_Counter_pkg is

  --| Declaration de types |--------------------------------------------------------
  -- Protected type for counters
  type shared_counter is protected
    procedure reset;
    procedure set_initial (initial_value: integer := 1);
    procedure increment (by: integer := 1);
    impure function value return integer;
  end protected shared_counter;

------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

end Shared_Counter_pkg;

package body Shared_Counter_pkg is

  --| Declaration de types |--------------------------------------------------------
  -- Protected type for counters
  type shared_counter is protected body
    variable count: integer := 0;
    variable initial: integer := 0;

    procedure reset is
    begin
      count := initial;
    end procedure reset;
    
    procedure set_initial (initial_value: integer := 1) is
    begin
       initial := initial_value;
    end procedure set_initial;

    procedure increment (by: integer := 1) is
    begin
      count := count + by;
    end procedure increment;

    procedure decrement (by: integer := 1) is
    begin
      count := count - by;
    end procedure decrement;

    impure function value return integer is
    begin
      return count;
    end function value;

    end protected body shared_counter;

end Shared_Counter_pkg;





