###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : REDS_sim_manuelle.tcl
# Description  : Script de lancement de la simulation manuelle
#                avec REDS_Console
# 
# Auteur       : Mike Meury 
#
# Utilise      : Simulation avec la console REDS
#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date       Description
# 1.0  MIM   15.12.2017 Creation
############################################################################

#create library work        
vlib work

#map library work to work
vmap work work

# Files compilation
vcom -reportprogress 300 -work work ../src_vhdl/Serialisation.vhd

#compile fichier tb, tester et console_sim 
vcom -reportprogress 300 -work work ../src_tb/console_sim_SER_ADR.vhd

#Chargement fichier pour la simulation
vsim work.console_sim 

#ajout signaux composant simuler dans la fenetre wave
add wave UUT/*

#lance la console REDS
do /opt/tools_reds/REDS_console.tcl

#ouvre le fichier format predefini
#do wave_timer_sim.do




