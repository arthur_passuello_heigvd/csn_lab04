-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : UC.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	-- Numeric
	use ieee.numeric_std.all;

	------------
	-- Entity --
	------------
	entity UC is
		port (
		    clock_i   : in   std_logic;
		    reset_i  : in   std_logic;
		    start_i   : in   std_logic;
		    rx_SDA_i  : in   std_logic;
			 det_flc_mt_i	: in  std_logic;
		    det_flc_des_i: in  std_logic;
			 wr_done_i		: in std_logic;
			 rd_done_i	: in std_logic;
			 
		    init_o	  : out  std_logic;
		    valide_o  : out  std_logic;
		    erreur_o  : out  std_logic;
		    com_SDA_o : out  std_logic;
		    en_wr_o   : out	 std_logic;
		    en_rd_o   : out  std_logic;
		    en_SCL_o  : out  std_logic;
			 cs_wr_SDA_o : out std_logic;
			 data_ready_o : out std_logic
		    );
	end entity UC;

	------------------
	-- Architecture --
	------------------
	architecture struct of UC is

		type state_t is (
			INIT,
			START,
			WR,
			START_WR,
			WAIT_WR,
			WAIT_ACK,
			CHECK_ACK,
			RD,
			WAIT_RD,
			WAIT_SCL,
			ERR,
			STOP_TRANS
			);
			
		signal cur_state, next_state 			   : state_t;
		signal init_s, valide_s, error_s, SDA_o_s 		: std_logic;
		signal en_wr_s, en_rd_s, en_SCL_s, cs_wr_SDA_s		: std_logic;
		signal  data_ready_s 						: std_logic := '0';

	begin

	process(cur_state, start_i, det_flc_des_i, det_flc_mt_i, rx_SDA_i, wr_done_i)
	begin
		init_s		<= '0';
		en_SCL_s 	<= '1'; -- Active most of the time
		SDA_o_s		<= '0';
		en_wr_s 	<= '0';
		en_rd_s 	<= '0';
		valide_s	<= '0';
		error_s 	<= '0';
		cs_wr_SDA_s <= '0';
		data_ready_s <= '0';
		next_state	<= INIT;
		case cur_state is
			when INIT =>
				SDA_o_s		<= '1';
				en_SCL_s 	<= '0';
				if(start_i = '1') then 
					next_state 		<= START;
				else
					next_state 		<= INIT;
				end if;
			when START =>
				SDA_o_s		<= '1';
				init_s 		<= '1';
				en_SCL_s		<= '0';
				next_state	<= START_WR;
			when START_WR =>
				cs_wr_SDA_s <= '1';
				if(det_flc_des_i = '1') then 
					next_state <= WR;
				else
					next_state <= START_WR;
				end if;
			when WR =>
				cs_wr_SDA_s <= '1';
				en_wr_s		<= '1';
				next_state 	<= WAIT_WR;	
			when WAIT_WR =>
				cs_wr_SDA_s <= '1';
				if(det_flc_des_i = '1') then
					if (wr_done_i = '1') then
						en_wr_s 			<= '1';
						next_state		<= WAIT_ACK;
					else
						next_state		<= WR;
					end if;
				else 
					next_state 		<= WAIT_WR;
				end if;
			when WAIT_ACK =>
				SDA_o_s		<= '1';
				if(det_flc_mt_i = '1') then
					next_state <= CHECK_ACK;
				else
					next_state <= WAIT_ACK;
				end if;
			when CHECK_ACK =>
				SDA_o_s		<= '1';
				if(rx_SDA_i = '0') then
					next_state <= WAIT_RD;
				else
					next_state <= ERR;
				end if;
			when RD =>
				SDA_o_s		<= '1';
				en_rd_s		<= '1';
				next_state  <= WAIT_RD;
			when WAIT_RD =>
				SDA_o_s		<= '1';
				if(det_flc_mt_i = '1') then
					if(rd_done_i = '1') then
						next_state 	<= WAIT_SCL;
					else
						next_state	<= RD;
					end if;
				else
					next_state <= WAIT_RD;
				end if;
			when WAIT_SCL =>
				next_state 		<= WAIT_SCL;
				if(det_flc_des_i = '1') then
					-- Prepare SDA for NoAck Master
					SDA_o_s		<= '0';
				end if;
				if(det_flc_mt_i = '1') then
						next_state	<= STOP_TRANS;
				end if;
			when ERR =>

				SDA_o_s 	<= '1'; 
				error_s		<= '1';
				data_ready_s <= '1';
				next_state	<= INIT;
			when STOP_TRANS =>
				-- NoAck Master
				SDA_o_s		<= '1';
				-- Signal transfer done correctly and go back to INIT
				valide_s	<= '1';
				data_ready_s <= '1';
				next_state	<= INIT;
		end case;
	end process;
	
	process(clock_i, reset_i)
	begin
		if(reset_i = '1') then
			cur_state 		<= INIT;
		elsif (rising_edge(clock_i)) then
		   cur_state		<= next_state;
		end if;
	end process;

	cs_wr_SDA_o 	<= cs_wr_SDA_s;
	init_o			<= init_s;
	valide_o		<= valide_s;
	erreur_o 		<= error_s;
	com_SDA_o		<= SDA_o_s;
	en_wr_o 		<= en_wr_s;
	en_rd_o 		<= en_rd_s;
	en_SCL_o 		<= en_SCL_s; 
	data_ready_o	<= data_ready_s;

end architecture struct;
