-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : CapT_I2C_top_tester.vhd
-- Description  : Test-Bench
--
-- Auteur       : YNG & Mike Meury
-- Date         : 23.01.18
-- Version      : 2.0
--
-- Utilise      : 
--
--| Modifications |-----------------------------------------------------------
--  Version   Auteur  Date            Description
--  1.0       YNG     xx.xx.2008      Test bench pour projet INSA
--  2.0       MIM     23.01.2018      Test bench pour la version 2018 pour MAXV
------------------------------------------------------------------------------

------------------------
-- Standard libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;

-----------------------
-- Project libraries --
-----------------------
library work;
use work.shared_counter_pkg.all;
use work.CapT_I2C_pkg.all;
use work.CapT_I2C_top_tb_pkg.all;

------------
-- Entity --
------------
entity CapT_I2C_top_tester is
    port(
        Clock_sti       : out std_logic;
        nReset_sti      : out std_logic;
        Start_sti       : out std_logic;
        Mode_Rd_nWr_sti : out std_logic;
        Nbr_Bytes_sti   : out std_logic_vector(1 downto 0);
        TX_Wr_sti       : out std_logic;
        TX_Adr_sti      : out std_logic_vector(2 downto 0);
        LM75_Adr_sti    : out std_logic_vector(2 downto 0);
        TX_Data_sti     : out std_logic_vector(7 downto 0);
        Temp_obs        : in  std_logic_vector(7 downto 0);
        Temp_Sign_obs   : in  std_logic;
        valide_obs      : in  std_logic;
        Err_obs         : in  std_logic
        );
end entity CapT_I2C_top_tester;

-------------------------
-- Architecture tester --
-------------------------
architecture Test_Bench of CapT_I2C_top_tester is

    -- constantes internes au test-bench
    constant Pulse_c   : time := 4 ns;
    constant Periode_c : time := 100 ns;
    constant Time_Out  : time := 8 * Periode_c;

    --signaux de detection des erreurs (check), propre a l'application
    --signal Erreur_s        : std_logic := '0';
    --signal Err_Process_Sti : std_logic := '0';
    --signal Err_Process_Ver : std_logic := '0';
    shared variable Nbr_Err_v : shared_counter;
    constant Nbr_Bytes_TX_c   : natural := 1;  --alway one EMI

    --signaux de base pour la simulation
    signal Horloge_s : std_logic;
    signal Sim_End_s : boolean := false;

    signal Num_Byte_debug : natural   := 0;

    --Valeur de reference pour la simulation
    signal Byte_Adr_ref   : std_logic_vector(7 downto 0);
    signal Byte_Data1_ref : std_logic_vector(7 downto 0);
    signal SDA_ref        : std_logic;
    signal Temp_BCD_ref   : std_logic_vector(7 downto 0);
    signal Temp_Demi_ref  : std_logic;
    signal Signe_ref      : std_logic;

    --Tableau de valeur pour la simulation
    type Type_Stimuli_I2C is            --definit une structure avec 4 bytes
    --pour tester I2C avec 4 bytes
    record
        -- Mode_Rd_nWr: uniquement lecture de 1 byte
        Mode_Rd_nWr  : std_logic;
        -- Nbr_Bytes uniquement 0 (soit 1 byte)
        Nbr_Bytes    : natural range 0 to 1;  -- 0 = one byte; 1 = two bytes
        Byte_Adr_c   : std_logic_vector(2 downto 0);  -- capt addr
        Byte_Data1_c : std_logic_vector(7 downto 0);  -- data
    end record;
    type Type_Tab_Stimuli_I2C is array (natural range <>) of Type_Stimuli_I2C;

    constant Tab_Sti_c : Type_Tab_Stimuli_I2C :=
        (
            ('1', 0, "000", x"00"),  -- Read temperature, expected 18
            ('1', 0, "111", x"00")   -- Read temperature, wrong address
            );

------------------------------------------------------------------
-- Procedure permettant plusieurs cycles d'horloge
-- Le premier appel de la procedure termine le cycle precedent si celui-ci
-- n'etait pas complet (par exemple : si on a fait quelques pas de 
-- simulation non synchronises avant, reset asynchrone, combinatoire, ...)
------------------------------------------------------------------
    procedure cycle (nombre_de_cycles : integer := 1) is
    begin
        for i in 1 to nombre_de_cycles loop
            wait until Falling_Edge(Horloge_s);
            wait for 2 ns;  --assigne stimuli 2ns apres flanc montant 
        end loop;
    end cycle;


begin

------------------------------------------------------------------
--Process de generation de l'horloge
------------------------------------------------------------------
    Clock_sti <= Horloge_s;
    process
    begin
        while not Sim_End_s loop
            Horloge_s <= '1',
                         '0' after Periode_Clock_Sys_c/2;
            wait for Periode_Clock_Sys_c;
        end loop;
        wait;
    end process;


----------------------------------------------------------------------
-- Generateur de stimuli
----------------------------------------------------------------------

    Gen_Sti : process
        variable Byte_Data_v      : std_logic_vector(7 downto 0);
        variable Temp_BCD_tmp_v   : signed(7 downto 0);
        variable Temp_BCD_v       : integer;
        variable Temp_obs_D_v     : integer;
        variable Temp_obs_U_v     : integer;
        variable Temp_BCD_ref_D_v : integer;
        variable Temp_BCD_ref_U_v : integer;

        variable Temp_v     : std_logic_vector(8 downto 0);
        variable Temp_abs_v : integer;
        variable Temp_tmp_v : integer;
        --variable Temp_Demi_v : std_logic_vector(0 downto 0);

        variable Sim_End_Txt_v : line;  -- Text at the end of simulation

    begin

        Nbr_Err_v.reset;                --Initialise compteur d'erreur

        -- Initialisation des ref
        SDA_ref        <= '-';
        Byte_Adr_ref   <= (others => '-');
        Byte_Data1_ref <= (others => '-');

        report ">>Debut de la simulation";
        -- initialisation => reset asynchrone       
        nReset_sti      <= '0';
        Start_sti       <= '0';
        Mode_Rd_nWr_sti <= '0';
        Nbr_Bytes_sti   <= (others => '0');
        TX_Wr_sti       <= '0';
        TX_Adr_sti      <= (others => '0');
        LM75_Adr_sti    <= (others => '0');
        TX_Data_sti     <= (others => '0');

        -- Error counter for CapT_Model
        CapT_Err_v.set_initial(-1);  -- We will intentionally raise an error.
        -- CapT_Err_v.reset;
--    Start_sti     <= '0';
--    Mode_Rd_nWr_sti    <= '0';
--    Nbr_Bytes_sti <= (others => '0');
--    I2C_Ack_Err_sti   <= '0';
--    TX_Wr_sti         <= '0';
--    TX_Adr_sti        <= '0';
--    TX_Data_sti       <= (others => '0');
--    LM75_Adr_sti       <= (others => '0');
--    --RX_Adr_sti        <= (others => '0');
        cycle(2);
        nReset_sti <= '1';
        cycle(2);

        --Simulation des cas du tableau
        for I_Tab in Tab_Sti_c'range loop
            report ">>Debut d'une transmission donnee dans le tableau de stimuli";

            -- supprimer test initial si master i2c pas pret!!!  EMI 12 janvier 2018  
            ---- si busy, attendre
            --  if valide_obs = '0' then
            --    wait until rising_edge(valide_obs);
            --  end if;

            --Chargement des valeurs dans les registres et preparation ref pour lecture
            LM75_Adr_sti <= Tab_Sti_c(I_Tab).Byte_Adr_c;
            for Reg_Adr in 0 to Nbr_Bytes_TX_c-1 loop
                if Reg_Adr = 0 then
                    TX_Adr_sti  <= std_logic_vector(to_unsigned(Reg_Adr, TX_Adr_sti'length));
                    TX_Data_sti <= Tab_Sti_c(I_Tab).Byte_Data1_c;  -- Donnee a stocker dans les reg1
                elsif Reg_Adr = 1 then -- impossible
                    TX_Adr_sti  <= std_logic_vector(to_unsigned(Reg_Adr, TX_Adr_sti'length));
                    --TX_Data_sti <= Tab_Sti_c(I_Tab).Byte_Data2_c;  -- Donnee a stocker dans les reg2
                end if;
                cycle;
                TX_Wr_sti <= '1', '0' after Periode_Clock_Sys_c;  -- Ecriture dans le Reg
                wait for 2*Periode_Clock_Sys_c;
            end loop;

            --Preparation valeur reference pour les bytes de data (lecture et ecriture)
            Byte_Adr_ref   <= "1001" & Tab_Sti_c(I_Tab).Byte_Adr_c & Tab_Sti_c(I_Tab).Mode_Rd_nWr;
            Byte_Data1_ref <= Tab_Sti_c(I_Tab).Byte_Data1_c;  --1er byte de data

            cycle;

            --Demarrage transmission
            Num_Byte_debug  <= 1;       --envoi adresse
            Mode_Rd_nWr_sti <= Tab_Sti_c(I_Tab).Mode_Rd_nWr;
            Nbr_Bytes_sti   <= std_logic_vector(to_unsigned(Tab_Sti_c(I_Tab).Nbr_Bytes, Nbr_Bytes_sti'length));
            SDA_ref         <= '1';
            Start_sti       <= '1';

            -- MIM : wait for status flag (both valide and erreur)
            wait until (rising_edge(valide_obs) or rising_edge(Err_obs));

            -- MIM : Verification donn�es re�ues
            if Err_obs = '1' then
                -- V�rification si la transaction donnait bien une erreur
                if Byte_Adr_ref /= ("10010001") then
                    -- On a bien un cas d'erreur
                    report "Erreur actif alors que adr faux : OK" severity note;
                else
                    -- Erreur dans le systeme
                    report "Erreur inactif alors que adr faux : not OK" severity note;
                    -- Increment compteur erreur
                    Nbr_Err_v.increment(1);
                end if;
            end if;
            if valide_obs = '1' then
                -- Verification si la transaction est correct
                if Byte_Adr_ref = ("10010001") then
                    -- Verification si la valeur affichee correspond
                    if Temp_obs = X"18" then
                        -- BCD correct                        
                        report "BCD correctement converti" severity note;
                    elsif Temp_obs = "00010010" then
                        -- Aff binaire correct
                        report "Temp OK mais pas convertie en BCD" severity note;
                    else
                        -- Temp faux
                        report "Temp fausse" severity note;
                        -- Increment compteur erreur
                        Nbr_Err_v.increment(1);
                    end if;
                end if;
            end if;
            
            Start_sti      <= '0';
            Num_Byte_debug <= 0;        --fin transfert
            cycle(20);
        end loop;

        write(Sim_End_Txt_v, string'(1 to 80 => '-') & LF & "         ");
        write(Sim_End_Txt_v, ">>Nombre d'erreur dans le tester : " & integer'image(Nbr_Err_v.value));
        write(Sim_End_Txt_v, LF & "         ");
        write(Sim_End_Txt_v, ">>Le capteur a d�tect� " & integer'image(CapT_Err_v.value) & " erreur(s) anormale(s).");

        if Nbr_Err_v.value /= 0 or CapT_Err_v.value /= 0 then
            write(Sim_End_Txt_v, LF & "         ");
            write(Sim_End_Txt_v, string'(">>Il reste encore quelque(s) probl�me(s) � r�soudre."));
        else
            write(Sim_End_Txt_v, LF & "         ");
            write(Sim_End_Txt_v, string'(">>Le design semble correct."));
        end if;

        write(Sim_End_Txt_v, LF & "         >>" & LF & "         ");
        write(Sim_End_Txt_v, string'(">>Fin de la simulation"));
        report Sim_End_Txt_v.all;

        Sim_End_s <= true;
        deallocate(Sim_End_Txt_v);      -- Free memory of Sim_End_Txt_v
        wait;                           --Attente infinie, stop la simulation

    end process;

end Test_Bench;

