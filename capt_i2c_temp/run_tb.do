###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : run_alu12_top_tb.tcl
# Description  : Script pour la simulation automatique
#                de l'alu 12 bits (alu12_top.vhd)
# 
# Auteur       : Etienne Messerli
# Date         : 15.03.2015
# Version      : 0.0
#
# Utilise      : Labo ALU avec 8 operations, unite CSNE
#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date        Description
# 1.0  EMI   24.03.2016  Ajout de signaux dans le wave
#                         
############################################################################

# files compilation
do ../comp_Capt_I2C_top.tcl

#test bench files compilation
vcom -reportprogress 300 -work work   ../src_tb/Shared_Counter_pkg.vhd
vcom -reportprogress 300 -work work   ../src_tb/CapT_I2C_top_tb_pkg.vhd
vcom -reportprogress 300 -work work   ../src_tb/CapT_I2C_model.vhd
vcom -reportprogress 300 -work work   ../src_tb/CapT_I2C_top_tester.vhd
vcom -reportprogress 300 -work work   ../src_tb/CapT_I2C_top_tb.vhd

#loading test bench
vsim -novopt work.CapT_I2C_top_tb

#ajout signaux composant simuler dans la fenetre wave
add wave UUT/*
#ajout signaux de reference et d'erreur
#add wave -divider "signaux références et erreur"
#add wave -position end  sim:/alu_top_tb/result_ref
#add wave -position end  sim:/alu_top_tb/dep_ref
#add wave -position end  sim:/alu_top_tb/zero_ref
#add wave -position end  sim:/alu_top_tb/erreur
#add wave -position end  sim:/alu_top_tb/nbr_err_s
#