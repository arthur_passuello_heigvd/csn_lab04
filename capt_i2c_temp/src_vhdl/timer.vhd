-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : timer.vhd
-- Auteur       : Etienne Messerli
-- 
-- Description  : Detection d'un clic et double clic
-- 
-- Utilise      : Labo CSN 2017
--| Modifications |------------------------------------------------------------
-- Ver   Date      Qui         Description
-- 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;



entity timer is
    port (
        clock_i    : in  std_logic;
        reset_i    : in  std_logic;
        en_SCL_i   : in  std_logic;
        
        scl_o	   : out std_logic
        );
end timer;

architecture comport of timer is

	signal cpt_pres_s, cpt_fut_s : unsigned(2 downto 0);
	signal det_zero_s		: std_logic;
	signal out_s, next_out_s: std_logic;
	
begin
	cpt_fut_s <= X"9"		when en_SCL_i = '0' or reset_i = '1' else
		     	 X"9" 			when cpt_pres_s = 9   else
								 cpt_pres_s - 1 ;
                 
    next_out_s		<= '1'		when en_SCL_i = '0' else
    			   not out_s 	when cpt_pres_s = 0 else
    			   out_s;

	process(clock_i, reset_i)
	begin
		if(reset_i = '1') then
			cpt_pres_s <= X"9";
		elsif Rising_Edge(clock_i) then
			cpt_pres_s <= cpt_fut_s;
			out_s		<= next_out_s;
		end if;
	end process;
	
	scl_o		<= out_s;
	

end comport;
