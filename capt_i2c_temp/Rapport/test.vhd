-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : CapT_I2C_top.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 05.12.17
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    05.12.17           Creation
-------------------------------------------------------------------------------

---------------
-- Libraries --
---------------
-- Standard
library ieee;
use ieee.std_logic_1164.all;
-- Numeric
use ieee.numeric_std.all;

--------------
-- Packages --
--------------
use work.CapT_I2C_pkg.all;

------------
-- Entity --
------------
entity CapT_I2C_top is
    port (
        start_i  : in    std_logic;
        adr_i    : in    std_logic_vector(2 downto 0);
        clock_i  : in    std_logic;
        nReset_i : in    std_logic;
        valide_o : out   std_logic;
        erreur_o : out   std_logic;
        scl_o    : out   std_logic;
        sda_io   : inout std_logic;
        temp_o   : out   std_logic_vector(7 downto 0);
        signe_o  : out   std_logic
        );
end entity CapT_I2C_top;

------------------
-- Architecture --
------------------
architecture struct of CapT_I2C_top is

component I2C_Master is
     port (
        clock_i   : in   std_logic;
        nReset_i  : in   std_logic;
        start_i   : in   std_logic;
        adr_i     : in   std_logic_vector(2 downto 0);
        rx_SDA_i  : in   std_logic;
        valide_o  : out  std_logic;
        erreur_o  : out  std_logic;
        scl_o     : out  std_logic;
        com_SDA_o : out  std_logic;
        data_o    : out  std_logic_vector(7 downto 0)
        );
end component;

	
	-- Bascule pour maintenir les leds
	process(clock_i, erreur_s, valide_s)
	begin
		if(rising_edge(clock_i)) then
			if(erreur_s = '1') then
				erreur_o <= '1';
				valide_o<= '0';
				temp_o	<= X"FF";
			elsif (valide_s = '1') then -- Forward du résultat quand valide est actif
				erreur_o <= '0';
				valide_o<= '1';
				temp_o	<= data_form_s;
				signe_o	<= signe_s;
			end if;
		end if;
	end process;


end architecture struct;
