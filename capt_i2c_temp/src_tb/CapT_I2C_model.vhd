-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : Capt_T_Model.vhd
-- Description  : Modele du capteur de temperature
--
-- Auteur       : YNG
-- Date         : 05.05.2008
-- Version      : 1.0
--
-- Utilise      : 
--
--| Modifications |-----------------------------------------------------------
-- Ver  Date        Qui  Commentaires
-- 1.0  see header  YNG  version initiale
-- 1.0  14.01.2013  GCD  correction tri-state => open collector
------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.ALL;
  use ieee.numeric_std.all;

-- librairies du projet  
library work;
  use work.capt_i2c_pkg.all;
  use work.CapT_I2C_top_tb_pkg.all;


entity CapT_I2C_model is
  port( 
    SCL_i   : in    std_logic; 
    SDA_io  : inout std_logic
  );
end entity CapT_I2C_model;

architecture Model of CapT_I2C_model is

  -- A definir par l'utilisateur --------------------------------------------------------
  --Constantes
  constant Adresse_c : Std_Logic_Vector(6 downto 0) := "1001000"; -- Adresse par defaut

  --Tableau de valeur pour la simulation
  type Registers is  array (0 to 3) of Std_logic_Vector(15 downto 0);
  signal Tab_Reg_s : Registers :=
    (
      ("00010010" & "11101001"), --Reg_Temp    = 18.5�C
      ("00000100" & "00000000"), --Reg_Config  =  4 /defaut
      ("01010000" & "00000000"), --Reg_Tos     = 80 /defaut
      ("01001011" & "00000000")  --Reg_Thyst   = 75 /defaut
    );
  ---------------------------------------------------------------------------------------

  constant Update_c  : Time := 0 ns; --Permet la mise � jour des signaux
  constant Stop_Cond_Timeout_c : Time := 2*Periode_Clock_Sys_c; -- Permet la mise � jour du signal Stop_Cond_s

  -- signaux internes
  signal Ptr_in_s  : Std_Logic_Vector(7 downto 0);
  signal Pointer_s : Natural := 0;
  signal Adresse_s : Std_Logic_Vector(7 downto 0);
  signal Stop_Cond_s : std_logic := '0';

  -- Open Collector
  signal En_OC_s : Std_Logic;
  signal SDA_i_s  : Std_Logic;
  signal SDA_o_s  : Std_Logic;

begin

  -- Open Collector
  SDA_i_s <= To_X01(SDA_io);  -- transformer �tat 'H' en '1' pour la simulation
  SDA_io  <= SDA_o_s when (En_OC_s = '1' and SDA_o_s = '0') else
             'Z';

  process
    variable Send_Data_v : Std_logic_Vector(15 downto 0); -- variable pour donnees a envoyer
    
-----------------------------------------------------------------------------------------
    -- Procedure d'acquitement du capteur pour le Maitre I2C --
    procedure Ack_by_LM75 (signal SCL_i    : in Std_Logic;
                           signal En_OC_s : out Std_Logic;
                           signal SDA_o_s  : out Std_Logic) is
    begin
      wait until Falling_Edge(SCL_i);
      En_OC_s <= '1';
      SDA_o_s <= '0';
      wait until Falling_Edge(SCL_i);
      En_OC_s <= '0';
    end procedure Ack_by_LM75;

-----------------------------------------------------------------------------------------
    -- Procedure d'acquitement du Maitre I2C pour le capteur --
    procedure Ack_by_Master (signal SCL_i    : in Std_Logic;
                             signal SDA_i_s  : in Std_logic;
                             signal En_OC_s : out Std_Logic;
                             signal SDA_o_s  : out Std_Logic) is
    begin
      En_OC_s <= '0';
      wait until Rising_Edge(SCL_i);
      if (SDA_i_s = '1') then
        report ">>LM75 : NoAck Master" severity Error;
        CapT_Err_v.increment; -- Global counter, shared with CapT_I2C_tester  **************************3
      else
        report ">>LM75 : Ack Master OK" severity Note;
      end if;
      wait until Falling_Edge(SCL_i);
      wait for 80 ns;
    end procedure Ack_by_Master;

-----------------------------------------------------------------------------------------
    -- Procedure d'acquitement du Maitre I2C pour le capteur --
    procedure NoAck_by_Master (signal SCL_i    : in Std_Logic;
                               signal SDA_i_s  : in Std_logic;
                               signal En_OC_s : out Std_Logic;
                               signal SDA_o_s  : out Std_Logic) is
    begin
      En_OC_s <= '0';
      wait until Rising_Edge(SCL_i);
      if (SDA_i_s = '1') then
        report ">>LM75 : NoAck Master OK" severity Note;
      else
        report ">>LM75 : Ack Master" severity Error;
        CapT_Err_v.increment; -- Global counter, shared with CapT_I2C_tester  ***************************
      end if;
      wait until Falling_Edge(SCL_i);
      wait for 80 ns;
    end procedure NoAck_by_Master;
-----------------------------------------------------------------------------------------

  procedure Test_STOP_Condition
  (
    signal SCL_i : in std_logic;
    signal SDA_i_s : in std_logic
  ) is
  begin
    wait until Rising_Edge(SCL_i);
    if SDA_i_s = '0' then
      -- Test if STOP condition occures (SDA must rise after SCL_i rose)
      wait until rising_edge(SDA_i_s) or falling_edge(SCL_i) for Stop_Cond_Timeout_c;
      if SDA_i_s = '1' and SCL_i = '1' then
        report ">>LM75 : Expected STOP condition occured, warning is normal" severity NOTE;
      else
        report ">>LM75 : Stop Condition did not occured" severity FAILURE;
      end if;
    end if;
  end procedure Test_STOP_Condition;

variable HLine : string(1 to 80) := (others => '-');

  begin
    -- Initialisation
    Ptr_in_s  <= (others => '-');
    Adresse_s <= (others => '-');
    SDA_o_s   <= '0';

    --Attente en mode input
    En_OC_s <= '0';

    --Debut d'une trame
    report HLine & LF & "         >>LM75 : Attente nouvelle trame" severity Note;
    wait until Falling_Edge(SCL_i);

    --Adresse Byte ----------------------------------------------------------------------
    for I in 7 downto 0 loop
      wait until Rising_Edge(SCL_i);
      Adresse_s(I) <= SDA_i_s;
    end loop;
    --Mise a jour du signal et affichage de la valeur recue
    wait for Update_c;
    report ">>LM75 : Adresse re�ue = " & integer'image(To_Integer(Unsigned(Adresse_s(7 downto 1)))) severity Note;

    if (Adresse_s(7 downto 1) /= Adresse_c) then
      report ">>LM75 : Adresse inconnue" severity Error;
      CapT_Err_v.increment;
    else
      report ">>LM75 : Adresse OK" severity Note;
      --Ack by LM75
      Ack_by_LM75(SCL_i, En_OC_s, SDA_o_s);

      if (Adresse_s(0) = '1') then
        --Read mode ---------------------------------------------------------------------
        report ">>LM75 : Reading mode" severity Note;

        En_OC_s <= '1';
        --Chargement des donnees a envoyer
        Send_Data_v := Tab_Reg_s(Pointer_s);

        if (Pointer_s /= 1) then  --Different de Reg_config(8bits)
          report ">>LM75 : Sending Byte MSB" severity Note;
          --Envoi Byte MSB
          for I in 15 downto 8 loop
            SDA_o_s  <= Send_Data_v(I);
            wait until Falling_Edge(SCL_i);
          end loop;

          --NoAck by master -- version 2018
          --Ack_by_Master(SCL_i, SDA_i_s, En_OC_s, SDA_o_s);
          NoAck_by_Master(SCL_i, SDA_i_s, En_OC_s, SDA_o_s);
          wait for 500 ns; -- Permet d'eviter l'activation combinee du Master et du capteur. Propre a l'UC du Master
          En_OC_s <= '0'; -- libere la ligne
        end if;

        --Envoi Byte LSB
        --report ">>LM75 : Sending Byte LSB" severity Note;
        --for I in 7 downto 0 loop
        --  SDA_o_s  <= Send_Data_v(I);
        --  wait until Falling_Edge(SCL_i);
        --end loop;

        ----Ack by master --- WRONG... Should be NoAck by master
        ----Ack_by_Master(SCL_i, SDA_i_s, En_OC_s, SDA_o_s);
        --NoAck_by_Master(SCL_i, SDA_i_s, En_OC_s, SDA_o_s);

        --Fin trame - TEST DE LA CONDITION STOP
        Test_STOP_Condition(SCL_i, SDA_i_s);

      else
        --Write mode ----------------------------------------------------------------------
        report ">>LM75 : Writing mode" severity Note;

        --Pointer Byte
        En_OC_s <= '0';
        for I in 7 downto 0 loop
          wait until Rising_Edge(SCL_i);
          Ptr_in_s(I) <= SDA_i_s;
        end loop;
        --Mise a jour du signal et affichage de la valeur recue
        wait for Update_c;
        Pointer_s <= To_integer(Unsigned(Ptr_in_s));
        wait for Update_c;
        report ">>LM75 : Pointeur re�u = " & integer'image(Pointer_s) severity Note;

        --Ack by LM75
        Ack_by_LM75(SCL_i, En_OC_s, SDA_o_s);
        -- Test if stop condition occured
        wait until Rising_Edge(SCL_i);
        wait for Stop_Cond_Timeout_c; -- Give "time" for Stop_Cond_s to change

        if Stop_Cond_s /= '1' then -- No STOP condition occured
          if (Pointer_s /= 1) then  --Different de Reg_config(8bits)
            --Recept Data Byte MSB
            report ">>LM75 : Receiving Byte MSB" severity Note;
            Tab_Reg_s(Pointer_s)(15) <= SDA_i_s;
            for I in 15-1 downto 8 loop
              wait until rising_edge(SCL_i);
              Tab_Reg_s(Pointer_s)(I) <= SDA_i_s;
            end loop;
            wait for Update_c;
            report ">>LM75 : Data MSB re�u = " &  integer'image(To_Integer(Unsigned(Tab_Reg_s(Pointer_s)(15 downto 8)))) severity Note;

            --Ack by LM75
            Ack_by_LM75(SCL_i, En_OC_s, SDA_o_s);
            -- Test if stop condition occured
            wait until Rising_Edge(SCL_i);
            wait for Stop_Cond_Timeout_c;
            if Stop_Cond_s = '1' then
              report ">>LM75 : Stop Condition occured" severity WARNING;
            end if;
          end if;

          if Stop_Cond_s /= '1' then -- No STOP condition occured
            --Recept Data Byte LSB
            report ">>LM75 : Receiving Byte LSB" severity Note;
            Tab_Reg_s(Pointer_s)(7) <= SDA_i_s;
            for I in 7-1 downto 0 loop
              wait until Rising_Edge(SCL_i);
              Tab_Reg_s(Pointer_s)(I) <= SDA_i_s;
            end loop;
            wait for Update_c;
            report ">>LM75 : Data LSB re�u = " & integer'image(To_Integer(Unsigned(Tab_Reg_s(Pointer_s)(7 downto 0)))) severity Note;

            --Ack by LM75
            Ack_by_LM75(SCL_i, En_OC_s, SDA_o_s);

            --Fin trame - TEST DE LA CONDITION STOP
            Test_STOP_Condition(SCL_i, SDA_i_s);
          end if;
          report ">>LM75 : End of transmission" severity Note;
        end if;
      end if;

      En_OC_s <= '0'; -- Libere la ligne

    end if;
  end process;

  -- Detection de la condition STOP
  Stop_Cond: process
    variable position_v : integer := 0;
  begin
    wait until rising_edge(SCL_i);
    if SDA_i_s = '0' then
      wait until rising_edge(SDA_i_s) or falling_edge(SCL_i) for Stop_Cond_Timeout_c;
      if SDA_i_s = '1' and SCL_i = '1' then
        Stop_Cond_s <= '1';
        report ">>LM75 : A STOP condition occured" severity WARNING;
        
      end if;
    end if;
    wait until falling_edge(SCL_i);
    Stop_Cond_s <= '0';
  end process;

end Model;

