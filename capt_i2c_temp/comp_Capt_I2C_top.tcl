###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : comp_Capt_I2C_top.tcl
# Description  : Script de compilation des fichiers pour
#                le master I2C pour capteur temperature LM75 
#                (Capt_I2C_top.vhd)
# 
# Auteur       : Etienne Messerli
# Date         : 08.01.2018
#
# Utilise      : Labo CSN capteur temperature I2C
#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date   Description
#                         
############################################################################

#create library work        
vlib work
#map library work to work
vmap work work

#files compilation
vcom -reportprogress 300 -work work   ../src_vhdl/CapT_I2C_pkg.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/I2C_Master.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/DESER_DATA.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/GESTION_SDA.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/SER_ADR.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/Timer_SCL_UT.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/Traitement_data.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/UC.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/CapT_I2C_top.vhd

