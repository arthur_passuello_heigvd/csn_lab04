-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : CapT_I2C_top_tb.vhd
-- Description  : Test-Bench
--
-- Auteur       : Etienne Messerli & Mike Meury
-- Date         : 23.01.2018
-- Version      : 1.0
--
-- Utilise      : CapT_I2C_top_tester.vhd
--                CapT_I2C_model.vhd
--
--| Modifications |-----------------------------------------------------------
-- Ver  Date        Qui  Commentaires
-- 0.0  08.01.18    EMI  version repris du tb utilisé pour TP INSA 2013
-- 1.0  23.01.18    MIM  Version reprise pour labo CSN 2017-18
------------------------------------------------------------------------------

------------------------
-- Standard Libraries --
------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

------------------------
-- Specific libraries --
------------------------
library work;
use work.shared_counter_pkg.all;
use work.CapT_I2C_pkg.all;
use work.CapT_I2C_top_tb_pkg.all;

-----------------------
-- Test bench entity --
-----------------------
entity CapT_I2C_top_tb is
-- Test bench do not have IO
end entity CapT_I2C_top_tb;

------------------
-- Architecture --
------------------
architecture struct of CapT_I2C_top_tb is

    -------------------------------
    -- UUT component declaration --
    -------------------------------
    component CapT_I2C_top is
        port (
            clock_i  : in    std_logic;
            nReset_i : in    std_logic;
            start_i  : in    std_logic;
            adr_i    : in    std_logic_vector(2 downto 0);
            valide_o : out   std_logic;
            erreur_o : out   std_logic;
            scl_o    : out   std_logic;
            sda_io   : inout std_logic;
            temp_o   : out   std_logic_vector(7 downto 0);
            signe_o  : out   std_logic
            );
    end component;

    ------------------------
    -- Tester declaration --
    ------------------------
    component CapT_I2C_top_tester is
        port(
            clock_sti       : out std_logic;
            nReset_sti      : out std_logic;
            start_sti       : out std_logic;
            mode_Rd_nWr_sti : out std_logic;
            nbr_bytes_sti   : out std_logic_vector(1 downto 0);
            TX_wr_sti       : out std_logic;
            TX_adr_sti      : out std_logic_vector(2 downto 0);
            LM75_adr_sti    : out std_logic_vector(2 downto 0);
            TX_data_sti     : out std_logic_vector(7 downto 0);
            temp_obs        : in  std_logic_vector(7 downto 0);
            temp_sign_obs   : in  std_logic;
            valide_obs      : in  std_logic;
            err_obs         : in  std_logic
            );
    end component;

    -----------------------
    -- Model declaration --
    -----------------------
    component CapT_I2C_model
        port(
            SCL_i  : in    std_logic;
            SDA_io : inout std_logic
            );
    end component;  -- CapT_I2C_model

    --------------------------------------
    -- Optional embedded configurations --
    --------------------------------------
    for all : CapT_I2C_top use entity work.CapT_I2C_top;
    for all : CapT_I2C_top_tester use entity work.CapT_I2C_top_tester;
    for all : CapT_I2C_model use entity work.CapT_I2C_model;

    ----------------------
    -- Internal signals --
    ----------------------
    signal clock_s       : std_logic;
    signal nReset_s      : std_logic;
    signal start_s       : std_logic;
    signal mode_Rd_nWr_s : std_logic;
    signal nbr_bytes_s   : std_logic_vector(1 downto 0);
    signal TX_adr_s      : std_logic_vector(2 downto 0);
    signal LM75_adr_s    : std_logic_vector(2 downto 0);
    signal TX_data_s     : std_logic_vector(7 downto 0);
    signal temp_8_s      : std_logic_vector(7 downto 0);
    signal temp_9_s      : std_logic_vector(7 downto 0);
    signal signe_s       : std_logic;
    signal valide_s      : std_logic;
    signal err_s         : std_logic;
    signal SCL_s         : std_logic;
    signal SDA_s         : std_logic;

    ---------------------
    -- Shared variable --
    ---------------------
    shared variable CapT_Err_v : shared_counter;  -- Error counter

begin  -- Architecture

    SDA_s <= 'H';

    ------------------
    -- UUT instance --
    ------------------
    UUT : CapT_I2C_top
        port map(
            clock_i  => clock_s,
            nReset_i => nReset_s,
            start_i  => start_s,
            adr_i    => TX_adr_s,
            temp_o   => temp_8_s,
            signe_o  => signe_s,
            valide_o => valide_s,
            erreur_o => err_s,
            SCL_o    => SCL_s,
            SDA_io   => SDA_s
            );

    ---------------------
    -- Tester instance --
    ---------------------
    Tester : CapT_I2C_top_tester
        port map (
            clock_sti       => clock_s,
            nReset_sti      => nReset_s,
            start_sti       => start_s,
            mode_Rd_nWr_sti => open,    --not used, read only
            nbr_Bytes_sti   => open,    --not used, one byte only
            TX_wr_sti       => open,    --not used (read only)
            TX_adr_sti      => TX_adr_s,
            LM75_adr_sti    => LM75_adr_s,
            TX_data_sti     => TX_data_s,
            temp_obs        => temp_8_s,
            temp_sign_obs   => signe_s,
            valide_obs      => valide_s,
            err_obs         => err_s
            );

    --------------------
    -- Model instance --
    --------------------
    CapT : CapT_I2C_model
        port map (
            SCL_i  => SCL_s,
            SDA_io => SDA_s
            );

end struct;
