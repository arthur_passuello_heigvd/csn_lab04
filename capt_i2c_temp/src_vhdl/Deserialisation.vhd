-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : DESER_DATA.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	-- Numeric
	use ieee.numeric_std.all;

	--------------
	-- Packages --
	--------------
	use work.CapT_I2C_pkg.all;

	------------
	-- Entity --
	------------
	entity Deserialisation is
		port (
		    clock_i   : in   std_logic;
		    reset_i	  : in   std_Logic;
		    rdx_SDA_i : in   std_logic;
		    init_i    : in   std_logic;
		    en_rd_i   : in   std_logic;

		    data_o 	  : out  std_logic_vector(7 downto 0);
		    readd_done_o : out std_logic;
		    );
	end entity Deserialisation;

	------------------
	-- Architecture --
	------------------
	architecture struct of Deserialisation is
	
		signal data_s			: std_logic_vector(7 downto 0) := X"00";
		signal next_bit_count, cur_bit_count		: unsigned (3 downto 0);


	begin

		
	process(clock_i, reset_i, init_i)
	begin
		if(reset_i = '1') then
				data_s <= X"00";
		elsif(init_i = '1') then
				data_s <= X"00";
				cur_bit_count <= "0000";
		elsif(rising_edge(clock_i)) then
			if(en_rd_i = '1') then
			data_s 	<= rdx_SDA_i & data_s(7 downto 1);
			cur_bit_count <= next_bit_count;
		end if;
	end if;
	end process;



	data_o <= data_s;
	next_bit_count <= cur_bit_count + 1 when en_rd_i = '1'  else
						 cur_bit_count;

	read_done_o 	<= '1' when cur_bit_count = 8 else '0';

	
	end architecture struct;
