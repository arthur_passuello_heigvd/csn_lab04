-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : CapT_I2C_pkg.vhd
--
-- Description  : 
-- 
-- Auteur       : Messerli
-- Date         : 23.03.2009
-- Version      : 0.0
-- 
-- Utilise      : Ce fichier est genere automatiquement par le logiciel 
--              : \"HDL Designer Series HDL Designer\".
-- 
--| Modifications |------------------------------------------------------------
-- Version   Auteur Date               Description
-- 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package CapT_I2C_pkg is

    --Frequence systeme
    constant Periode_Clock_Sys_c : time := 1 us;  --1 MHz

    --Frequence liaison I2C: 100 KHz  (max I2C: 400 Khz) 
    constant Periode_I2C_c : time := 10 us;

    --Adresse de base du composant LM75
    constant Addr_LM75_c : std_logic_vector(6 downto 0) := "1001000";

    --Format de la temperature de sortie
    constant Bin_nBCD : std_logic := '1';

    --Calcul attente demi-periode SCL pour I2C
    --rajoute +1 pour forcer un arrondi du calcul vers le haut
    constant Nbr_Att_c : positive := ((Periode_I2C_c/Periode_Clock_Sys_c)+1)/2;

    --le nombre de bit du timer sera le logarithme en base 2 de Nbr_Att_c
    --  a faire dans le fichier du timer :
    --  taille vecteur timer: Nbr_Timer_c : Positive := ilogup(Nbr_Att_c)

    -- integer logarithm (rounded up) [MR version]
    function ilogup (x : natural; base : natural := 2) return natural;

    -- integer logarithm (rounded down) [MR version]
    function ilog (x : natural; base : natural := 2) return natural;

end CapT_I2C_pkg;

----------
-- Body --
----------
package body CapT_I2C_pkg is

    -- integer logarithm (rounded up) [MR version]
    function ilogup (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y := 1;                         --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        return y;
    end ilogup;

    -- integer logarithm (rounded down) [MR version]
    function ilog (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y := 1;                         --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        if x < base**y then
            y :=y-1;
        end if;
        return y;
    end ilog;

end CapT_I2C_pkg;
