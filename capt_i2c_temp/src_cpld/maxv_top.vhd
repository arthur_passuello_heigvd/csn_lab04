------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : maxv_top.vhd
-- Author               : Gilles Curchod
-- Date                 : 28.05.2013
-- Target Devices       : Altera MAXV 5M570ZF256C5
--
-- Context              : Max_V_Board Project : Hardware bring-up
--
------------------------------------------------------------------------------------------
-- Description :
--   Top of the CPLD
------------------------------------------------------------------------------------------
-- Information :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver   Date        Engineer     Chnages
-- 0.0   See header  GCD          Initial version
-- 1.0   25.09.2014  EMI          Adaptation to use for CSN lab 
-- 2.0   11.12.2017  MIM          Adapted for MSS I2C temperature sensor
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use work.maxv_pkg.all;

entity maxv_top is
    port(
        --| Clocks, Reset |-------------------------------------------------------------------
        Clk_Gen_i    : in    std_logic;                    -- CLK_GEN
        Clk_Main_i   : in    std_logic;                    -- CLK_MAIN
        --| Inout devices |-------------------------------------------------------------------
        Con_25p_io   : inout std_logic_vector(25 downto 1);  -- CON_25P_*
        Con_80p_io   : inout std_logic_vector(79 downto 2);  -- CON_80P_*
        Mezzanine_io : inout std_logic_vector(20 downto 5);  -- MEZZANINE_*
        --| Input devices |-------------------------------------------------------------------
        Encoder_A_i  : in    std_logic;                    -- ENCODER_A
        Encoder_B_i  : in    std_logic;                    -- ENCODER_B
        nButton_i    : in    std_logic_vector(8 downto 1);   -- NBUTTON_*
        nReset_i     : in    std_logic;                    -- NRESET
        Switch_i     : in    std_logic_vector(7 downto 0);   -- SWITCH_*
        --| Output devices |------------------------------------------------------------------
        nLed_o       : out   std_logic_vector(7 downto 0);   -- NLED_*
        Led_RGB_o    : out   std_logic_vector(2 downto 0);   -- LED_RGB_*
        nSeven_Seg_o : out   std_logic_vector(7 downto 0)  -- NDSP_SEG (dp, g downto a)
        );
end maxv_top;

architecture struct of maxv_top is


    --| Intermediate signals |--------------------------------------------------------------
    signal Reset_s : std_logic;

    signal Con_25p_DI_s   : std_logic_vector(Con_25p_io'range);
    signal Con_25p_DO_s   : std_logic_vector(Con_25p_io'range);
    signal Con_25p_OE_s   : std_logic;
    signal Con_80p_DI_s   : std_logic_vector(Con_80p_io'range);
    signal Con_80p_DO_s   : std_logic_vector(Con_80p_io'range);
    signal Con_80p_OE_s   : std_logic;
    signal Mezzanine_DI_s : std_logic_vector(Mezzanine_io'range);
    signal Mezzanine_DO_s : std_logic_vector(Mezzanine_io'range);
    signal Mezzanine_OE_s : std_logic;
    signal Button_s       : std_logic_vector(nButton_i'range);
    signal Led_s          : std_logic_vector(nLed_o'range);
    signal Seven_Seg_s    : std_logic_vector(nSeven_Seg_o'range);  -- order: dp, g f e d c b a

    --| Internal signals |------------------------------------------------------------------
    signal cpt_blink      : unsigned(19 downto 0);
    signal cpt_ms_s       : unsigned(9 downto 0);
    signal top_ms_s       : std_logic;
    signal debounce_cpt   : unsigned (7 downto 0);
    signal button_sync1_s : std_logic;
    signal button_sync2_s : std_logic;
    signal diff_button_s  : std_logic;
    signal button_fltr_s  : std_logic;
    signal temp_s         : std_logic_vector(7 downto 0);

    --| Components declaration |------------------------------------------------------------
    component CapT_I2C_top is
        port (
            start_i  : in    std_logic;
            adr_i    : in    std_logic_vector(WIDTH_ADDR_G-1 downto 0);
            clock_i  : in    std_logic;
            nReset_i : in    std_logic;
            valide_o : out   std_logic;
            erreur_o : out   std_logic;
            scl_o    : out   std_logic;
            sda_io   : inout std_logic;
            temp_o   : out   std_logic_vector(WIDTH_TEMP_G-1 downto 0);
            signe_o  : out   std_logic
            );
    end component;

begin

    -----------------------
    -- Inputs processing --
    -----------------------
    Reset_s  <= not nReset_i;
    Button_s <= not nButton_i;

    ----------------------------------------------------------------------------------
    --| Filtrage du bouton SW1 |------------------------------------------------------
    process (Clk_Main_i, Reset_s)
    begin
        if Reset_s = '1' then
            button_sync1_s <= '0';
            button_sync2_s <= '0';
            button_fltr_s  <= '0';
        elsif rising_edge(Clk_Main_i) then
            button_sync1_s <= Button_s(1);
            button_sync2_s <= button_sync1_s;
            if debounce_cpt = 0 then    -- si Button(1) stable => mise a jour
                button_fltr_s <= button_sync2_s;
            end if;
        end if;
    end process;

    diff_button_s <= button_sync1_s xor button_sync2_s;

    process (Clk_Main_i, Reset_s)
    begin
        if Reset_s = '1' then
            debounce_cpt <= (others => '0');
        elsif rising_edge(Clk_Main_i) then
            if diff_button_s = '1' then           -- load lorsque diff_button_s
                debounce_cpt <= (others => '1');  --charge valeur max
            elsif debounce_cpt /= 0 then
                debounce_cpt <= debounce_cpt -1;
            end if;
        end if;
    end process;

    ------------------------
    -- Outputs processing --
    ------------------------
    nLed_o       <= not Led_s;
    nSeven_Seg_o <= not Seven_Seg_s;

    --------------------
    -- Unused outputs --
    --------------------
    Led_RGB_o                                <= (others => '0');
    Seven_Seg_s(Seven_Seg_s'high-1 downto 0) <= (others => '0');
    Seven_Seg_s(Seven_Seg_s'high)            <= cpt_blink(cpt_blink'high);  -- decimal point blink at 1Hz
    Led_s(6 downto 2)                        <= (others => '0');  --unused leds turned off

    -----------------------------
    -- Component instanciation --
    -----------------------------
    U1 : CapT_I2C_top
        port map(
            start_i  => button_fltr_s,
            adr_i    => Switch_i(2 downto 0),
            clock_i  => Clk_Main_i,
            nReset_i => nReset_i,
            valide_o => Led_s(0),
            erreur_o => Led_s(1),
            scl_o    => Mezzanine_io(5),
            sda_io   => Mezzanine_io(7),  -- connected as inout on mezzanine
            temp_o   => temp_s,
            signe_o  => Led_s(7)
            );

    ---------------------
    -- Console outputs --
    ---------------------
    -- Assignation
    Con_25p_io(24 downto 17) <= temp_s;  -- 7seg
    Con_25p_io(8 downto 1)   <= Led_s;   -- same led as on maxv board

    --------------------------
    -- Blinking signal @1Hz --
    --------------------------
    process (Clk_Main_i, Reset_s)
    begin
        if Reset_s = '1' then
            cpt_blink <= (others => '0');
        elsif rising_edge(Clk_Main_i) then
            cpt_blink <= cpt_blink + 1;
        end if;
    end process;

end struct;

