-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : Timer_SCL_UC.vhd
-- Description  : 
--
-- Author       : Arthur Passuello && Ludovic Richard
-- Date         : 22.01.18
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       LRD    22.01.18           Creation
-------------------------------------------------------------------------------

---------------
-- Libraries --
---------------
-- Standard
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
-- Numeric
use ieee.numeric_std.all;

--------------
-- Packages --
--------------
use work.CapT_I2C_pkg.all;

------------
-- Entity --
------------
entity Timer is
    port (
        clock_i   : in  std_logic;
		  reset_i   : in  std_logic;
		  init_i		: in	std_logic;
        en_scl_i  : in  std_logic;
        
        scl_o     	: out  std_logic;
		  det_flc_mt_o	: out  std_logic;
		  det_flc_des_o: out  std_logic
        );
end entity Timer;

------------------
-- Architecture --
------------------
architecture struct of Timer is

	signal cpt_s : std_logic_vector(2 downto 0) := "000";
	signal scl_s : std_logic		:= '0';
	signal det_flc_mt_s, det_flc_des_s		: std_logic;

begin

	process(clock_i, reset_i, en_scl_i)
	begin
		if(reset_i = '1') then
			cpt_s <= "000";
			scl_s <= '1';
		elsif(rising_edge(clock_i) and en_scl_i = '1') then
			det_flc_des_s <= '0';
			det_flc_mt_s <= '0';
				if(cpt_s = "000") then
					cpt_s <= "101";
					det_flc_mt_s <= not scl_s;
					det_flc_des_s<= scl_s;
					scl_s <= not scl_s;
				else
					cpt_s <= cpt_s - 1;
				end if;
				cpt_s <= "000";
				scl_s <= '1';
		end if;
	end process; 
	
	scl_o <= scl_s;
	det_flc_mt_o	<= det_flc_mts_s;
	det_flc_des_o	<= det_flc_des_s;

end architecture struct;
