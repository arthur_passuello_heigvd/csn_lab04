-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : I2C_Master.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 05.12.17
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    05.12.17           Creation
-------------------------------------------------------------------------------

---------------
-- Libraries --
---------------
-- Standard
library ieee;
use ieee.std_logic_1164.all;
-- Numeric
use ieee.numeric_std.all;

--------------
-- Packages --
--------------
use work.CapT_I2C_pkg.all;

------------
-- Entity --
------------
entity I2C_Master is
    port (
        clock_i   : in   std_logic;
        nReset_i  : in   std_logic;
        start_i   : in   std_logic;
        adr_i     : in   std_logic_vector(2 downto 0);
        rx_SDA_i  : in   std_logic;
        valide_o  : out  std_logic;
        erreur_o  : out  std_logic;
        scl_o     : out  std_logic;
        com_SDA_o : out  std_logic;
        data_o    : out  std_logic_vector(7 downto 0);
		  data_ready_o : out std_logic
        );
end entity I2C_Master;

------------------
-- Architecture --
------------------
architecture struct of I2C_Master is

  component Serialisation is
     port (
		    clock_i   : in   std_logic;
		    reset_i	  : in   std_Logic;
		    addr_i	  : in   std_logic_vector(2 downto 0);
		    init_i    : in   std_logic;
		    en_wr_i   : in   std_logic;

		    wr_SDA_o : out  std_logic;
			 write_done_o : out std_logic
		    );
end component;

component Deserialisation is
     port (
		    clock_i   : in   std_logic;
		    reset_i	  : in   std_Logic;
		    rdx_SDA_i : in   std_logic;
		    init_i    : in   std_logic;
		    en_rd_i   : in   std_logic;

		    data_o 	  : out  std_logic_vector(7 downto 0);
		    read_done_o:out std_logic
		    );
end component;

component UC is
     port (
		    clock_i   : in   std_logic;
		    reset_i  : in   std_logic;
		    start_i   : in   std_logic;
		    rx_SDA_i  : in   std_logic;
			 det_flc_mt_i : in std_logic;
			 det_flc_des_i: in std_Logic;
			 wr_done_i		: std_logic;
			 rd_done_i	: in std_logic;
			 
		    init_o	  : out  std_logic;
		    valide_o  : out  std_logic;
		    erreur_o  : out  std_logic;
		    com_SDA_o : out  std_logic;
		    en_wr_o   : out	 std_logic;
		    en_rd_o   : out  std_logic;
		    en_SCL_o  : out  std_logic;
			 cs_wr_SDA_o:out std_logic;
			 data_ready_o: out std_logic
		    );
end component;

component Timer is
     port (
		    clock_i   : in  std_logic;
			 reset_i   : in  std_logic;
		    init_i		: in	std_logic;
          en_scl_i  : in  std_logic;
		  
          scl_o     : out  std_logic;
			 det_flc_mt_o	: out  std_logic;
		    det_flc_des_o: out  std_logic
		    );
end component;

	signal reset_s, en_SCL_s, init_s		 		: std_logic;
	signal en_wr_s, en_rd_s, wr_SDA_UC_s 		: std_logic;
	signal wr_SDA_SER_s, scl_s, data_ready_s	: std_logic;
	signal det_flc_des_s, det_flc_mt_s			: std_logic;
	signal start_s, old_start_s					: std_logic;
	signal write_done_s, rd_done_s,cs_wr_SDA_s: std_logic := '0';


begin

	reset_s <= not nReset_i;
	start_s		<= start_i;
	
	

    UC_C : UC 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         reset_i	    => reset_s,  -- JUSTE MAIS VOILA
         start_i   	=> start_i,
        	rx_SDA_i  	=> rx_SDA_i,
			det_flc_mt_i=> det_flc_mt_s,
		   det_flc_des_i=> det_flc_des_s,
			wr_done_i	=> write_done_s,
			rd_done_i   => rd_done_s,
			
			init_o		=> init_s,
        	valide_o  	=> valide_o,
        	erreur_o  	=> erreur_o,
        	COM_SDA_o  	=> wr_SDA_UC_s,
        	en_wr_o 		=> en_wr_s,
        	en_rd_o    	=> en_rd_s,
			en_SCL_o		=> en_SCL_s,
			cs_wr_SDA_o => cs_wr_SDA_s,
			data_ready_o=> data_ready_s
	);
	
	 SER : Serialisation 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         reset_i	    => reset_s,  -- JUSTE MAIS VOILA
			addr_i	  	 => adr_i,
		   init_i    	 => init_s,
		   en_wr_i		 => en_wr_s,
			
		   wr_SDA_o		=> wr_SDA_SER_s,
			write_done_o  => write_done_s
	);
	
	DESER : Deserialisation 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         reset_i	    => reset_s,  -- JUSTE MAIS VOILA
			rdx_SDA_i    => rx_SDA_i,
			init_i    	 => init_s,
		   en_rd_i  	 => en_rd_s,

		   data_o 		 => data_o,
		   read_done_o	 => rd_done_s
	);
	
	TIMER : Timer 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         reset_i	    => reset_s,  -- JUSTE MAIS VOILA
			init_i		 => init_s,
         en_scl_i		 => en_SCL_s,
		  
         scl_o 		 => scl_s,
			det_flc_mt_o => det_flc_mt_s,
		   det_flc_des_o=> det_flc_des_s
	);

			
        scl_o     	<= scl_s;
		  data_ready_o <= data_ready_s;
        com_SDA_o 	<= wr_SDA_SER_s when cs_wr_SDA_s = '1' else wr_SDA_UC_s;
        
end architecture struct;
