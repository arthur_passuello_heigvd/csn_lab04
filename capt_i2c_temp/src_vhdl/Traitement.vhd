-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : traitement_data.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	-- Numeric
	use ieee.numeric_std.all;

	--------------
	-- Packages --
	--------------
	use work.CapT_I2C_pkg.all;

	------------
	-- Entity --
	------------
	entity Traitement is
		port (
		    data_i		: in	std_logic_vector(7 downto 0);
			 
			temp_o		: out	std_logic_vector(7 downto 0);
			signe_o		: out	std_logic
		    );
	end entity Traitement;

	------------------
	-- Architecture --
	------------------
	architecture struct of Traitement is
	
		signal neg_data_s			: std_logic_vector(7 downto 0);

	begin
	neg_data_s <= std_logic_vector(unsigned((not data_i )) + 1);
			
	temp_o <= data_i when data_i(7) = '0' else neg_data_s;
	signe_o <= data_i(7);

	end architecture struct;
