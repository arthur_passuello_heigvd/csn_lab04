-------------------------------------------------------------------------------
	-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
	-- Institut REDS, Reconfigurable & Embedded Digital Systems
	--
	-- File         : traitement_data.vhd
	-- Description  : 
	--
	-- Author       : Arthur Passuello && Ludovic Richard
	-- Date         : 18.12.17
	-- Version      : 0.0
	--
	-- Dependencies : 
	--
	--| Modifications |------------------------------------------------------------
	-- Version   Author Date               Description
	-- 0.0       MIM    18.12.17           Creation
	-------------------------------------------------------------------------------

	---------------
	-- Libraries --
	---------------
	-- Standard
	library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	-- Numeric
	use ieee.numeric_std.all;

	--------------
	-- Packages --
	--------------
	use work.CapT_I2C_pkg.all;

	------------
	-- Entity --
	------------
	entity traitement_data is
		port (
		    clock_i		: in	std_logic;
		    reset_i		: in	std_Logic;
		    data_i		: in	std_logic_vector(7 downto 0);
			 
			 temp_o	: out	std_logic_vector(7 downto 0);
			 signe_o		: out	std_logic
		    );
	end entity traitement_data;

	------------------
	-- Architecture --
	------------------
	architecture struct of traitement_data is
	
		signal neg_s			: std_logic_vector(7 downto 0);

	begin
	
	neg_s <= data_i xor X"F";
		
	process(clock_i, reset_i)
	begin
		if(rising_edge(clock_i)) then
			neg_s <= neg_s + 1;
		end if;
	end process;

	temp_o <= data_i when data_i(7) = '0' else neg_s;
	signe_o <= data_i(7);

	end architecture struct;
