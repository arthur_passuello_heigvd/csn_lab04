-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : CapT_I2C_top.vhd
-- Description  : 
--
-- Author       : Mike Meury
-- Date         : 05.12.17
-- Version      : 0.0
--
-- Dependencies : 
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       MIM    05.12.17           Creation
-------------------------------------------------------------------------------

---------------
-- Libraries --
---------------
-- Standard
library ieee;
use ieee.std_logic_1164.all;
-- Numeric
use ieee.numeric_std.all;

--------------
-- Packages --
--------------
use work.CapT_I2C_pkg.all;

------------
-- Entity --
------------
entity CapT_I2C_top is
    port (
        start_i  : in    std_logic;
        adr_i    : in    std_logic_vector(2 downto 0);
        clock_i  : in    std_logic;
        nReset_i : in    std_logic;
        valide_o : out   std_logic;
        erreur_o : out   std_logic;
        scl_o    : out   std_logic;
        sda_io   : inout std_logic;
        temp_o   : out   std_logic_vector(7 downto 0);
        signe_o  : out   std_logic
        );
end entity CapT_I2C_top;

------------------
-- Architecture --
------------------
architecture struct of CapT_I2C_top is

component I2C_Master is
     port (
        clock_i   : in   std_logic;
        nReset_i  : in   std_logic;
        start_i   : in   std_logic;
        adr_i     : in   std_logic_vector(2 downto 0);
        rx_SDA_i  : in   std_logic;
        valide_o  : out  std_logic;
        erreur_o  : out  std_logic;
        scl_o     : out  std_logic;
        com_SDA_o : out  std_logic;
        data_o    : out  std_logic_vector(7 downto 0);
		  data_ready_o : out std_logic
        );
end component;

component Traitement is
     port (
		    clock_i		: in	std_logic;
		    reset_i		: in	std_Logic;
		    data_i		: in	std_logic_vector(7 downto 0);
			 
			temp_o		: out	std_logic_vector(7 downto 0);
			signe_o		: out	std_logic
		    );
end component;

component Gestion_sda is
     port (
		    com_SDA_i : in   std_logic;
			 
			sda_io	  : inout  std_Logic;
			rdx_sda_o : out  std_Logic
		    );
end component;

	signal sda_i_s, sda_o_s		: std_logic;
	signal data_ready_s, det_st: std_logic;
	signal valide_s, erreur_s	: std_logic;
	signal data_s, data_form_s	: std_logic_vector(7 downto 0);
	signal signe_s, reset_s		: std_logic;
	signal start_s, old_start_s : std_logic;
	
begin
	reset_s <= not nReset_i;
	start_s <= start_i;
	det_st <= start_s and not old_start_s;

	-- FAIRE MAINTIENT DE erreur ET VALIDE TAVU
    Master : I2C_Master 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         	nReset_i	=> nReset_i,  --reset asynchrone
         	start_i   	=> det_st,
        	adr_i     	=> adr_i,
        	rx_SDA_i  	=> sda_i_s,
        	valide_o  	=> valide_s,
        	erreur_o  	=> erreur_s,
        	scl_o     	=> scl_o,
        	com_SDA_o 	=> sda_o_s,
        	data_o    	=> data_s,
			data_ready_o=> data_ready_s
	);
	
	Traitement : Traitement	 
	port map (
			clock_i	    => clock_i,  --horloge systeme 1MHz
         	reset_i		=> reset_s,  
         	data_i		=> data_s,
         	temp_o	=> data_form_s,
         	signe_o		=> signe_s
	);
	
	SDA : Gestion_sda 
	port map (
			com_SDA_i	=> sda_o_s,
			sda_io		=> sda_io,
			rdx_sda_o	=> sda_i_s
	);
	
	-- Bascule pour maintenir les leds
	process(clock_i, erreur_s, valide_s, start_i)
	begin
		if(rising_edge(clock_i)) then
			if(data_ready_s = '1') then
				erreur_o <= erreur_s;
				valide_o<= valide_s;
				temp_o	<= data_form_s;
				signe_o <= signe_s;
			elsif (start_s = '1' and old_start_s = '0') then 
				erreur_o <= '0';
				valide_o<= '0';
				temp_o	<= X"00";
				signe_o	<= '0';
			end if;
			old_start_s <= start_s;
		end if;
	end process;

end architecture struct;
