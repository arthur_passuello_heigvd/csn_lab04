-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- Institut REDS
--
-- Fichier :  CapT_I2C_top_tb_pkg.vhd
-- Auteur  :  G. Curchod
-- Date    :  16.07.2010
--
-- Utilise dans   : Labo CSF
--                   (Master I2C)
-----------------------------------------------------------------------
-- Fonctionnement vu de l'exterieur : 
--   Paquetage pour CapT_I2C
--
-----------------------------------------------------------------------
-- Ver  Date        Qui  Commentaires
-- 0.0  16.07.2010  GCD  Version initiale pour le projet Capt_T_PCI
-- 1.0  08.01.2018  EMI  Modifie nom du pakage
--
-----------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
library work;
use work.shared_counter_pkg.all;

package CapT_I2C_top_tb_pkg is

  -- Shared error counter for CapT
  shared variable CapT_Err_v : shared_counter;


end CapT_I2C_top_tb_pkg;

package body CapT_I2C_top_tb_pkg is

end CapT_I2C_top_tb_pkg;
